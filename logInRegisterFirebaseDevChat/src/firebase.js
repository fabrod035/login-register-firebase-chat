import firebase from "firebase/app";
import "firebase/auth";
import "firebase/database";
import "firebase/storage";


var firebaseConfig = {
    apiKey: "AIzaSyAafDmY4vb8g6r87r-teKZ5sMnTnKqgGrA",
    authDomain: "react-clack-clone-4db6f.firebaseapp.com",
    databaseURL: "https://react-clack-clone-4db6f.firebaseio.com",
    projectId: "react-clack-clone-4db6f",
    storageBucket: "react-clack-clone-4db6f.appspot.com",//firebase Storage url
    messagingSenderId: "625153519847",
    appId: "1:625153519847:web:0b82a5667ed1d90d7fb242"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);


  export default firebase;
